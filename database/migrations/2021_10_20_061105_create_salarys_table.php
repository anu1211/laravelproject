<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarys', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('name');
            $table->integer('accouontNo'); 
            $table->integer('year');
            $table->integer('month');
            $table->integer('basicPay');
            $table->integer('bonus');
            $table->integer('commutation');
            $table->integer('da');
            $table->integer('hra'); 
            $table->integer('ca'); 
            $table->integer('it'); 
            $table->integer('esi'); 
            $table->integer('pf'); 
            $table->integer('pt'); 
            $table->integer('medicalClaim'); 
            $table->integer('netPay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarys');
    }
}
